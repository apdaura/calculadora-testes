export default class Calculadora{
    static soma(a, b){
        return a + b
    }
    static sub(a, b){
        return a - b
    }
    static mult(a, b){
        return a * b
    }

    static div(a, b){
        if (b === 0)
           return 'Não é possível dividir por zero'; 
        return a / b
    }
}