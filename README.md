# Calculadora em JavaScript e Testes com Mocha e Chai  
<p align="center">
 <a href="#descricao">Descrição</a> •
 <a href="#obj">Objetivo</a> • 
 <a href="#testes">Testes</a> • 
 <a href="#bug">Bugs</a>•
 <a href="#tec">Tecnologias</a>•
 <a href="#config">Configuração</a> • 
 <a href="#autor">Autor</a>
</p>

<h2 id="descricao">Descrição</h2>

<p>Calculadora feita usando a técnica de desenvolvimento ágil Test Driven Development (TDD) e testes de unidade com a estrutura de teste Mocha e a biblioteca Chai. Esta técnica de desenvolvimento é baseada em fazer teste para descrever o que você quer que aconteça na calculadora e em seguida, corrijir o código para fazer esse teste passar.</p>  

<h2 id='obj'>Objetivo</h2>   
<p>
O objetivo da criação desta calculadora foi exercitar os conteúdos aprendidos.<br>

Implementar o uso da linguagem JavaScript, da técnica TDD, da estrutura de teste Mocha e da bibliotecas Chai e compreender melhor o uso das mesmas.<br>

Observar como se dá a criação de um aplicativo a partir de testes de melhorias sugeridos, para torna-lo mais objetivo e já testável desde o início.<br>    

Exercitar a realização de testes exploratórios e o "ir além" dos testes obvios.<br>

Apresentar os resultados ao final da sprint 4, do programa de bolsa da Compass UOL e receber orientações sobre possíveis melhorias.</p>    

<h2 id="testes">Testes</h2>
Testes feitos em /test/calculadora_spec.js<br>
<br>    
<strong>As seguintes funções testadas:</strong><br>    
• Calculadora.soma() <br>   
• Calculadora.sub()  <br>  
• Calculadora.div()  <br>  
• Calculadora.mult()  <br> 
 <br>
<strong>Também foram feitos testes para ver se várias operações podem ser encadeadas:</strong><br>   
• Calculadora.div() e Calculadora.mult()  <br>  
• Calculadora.mult(Decimal) e Calculadora.sub(Resultado da primeira conta)  <br>  
• Calculadora.div e Calculadora.sub(0)   <br> 
• Calculadora.mult(9, 9) - várias vezes para ver se exibe um número muito grande. <br>   
• Calculadora.div(9, 9) -  várias vezes para ver se exibe um número muito pequeno. <br>   
• Calculadora.div e Calculadora.soma e Calculadora.mult e Calculadora.sub  <br>  

Totalizando 25 testes.

<h2 id="bug">Bugs</h2>  
<strong>Bugs encontrados em /Bug</strong><br>
<br> 
Divisão por 0 - resolvido  <br>    
   

<h2 id="tec">Tecnologias</h2>
- [VsCode](https://code.visualstudio.com/) <br>     
- [Node.js](https://nodejs.org/en/)    <br>  
- [JavaScript](https://www.javascript.com/) <br>     
- [Mocha](https://mochajs.org/) <br>   
- [Chai](https://www.chaijs.com/) <br>     

<h2 id="config">Configuração</h2>  
<strong>Pré-requisitos</strong><br>
<br>
Primeiro precisamos instalar algumas ferramentas: [Node.js](https://nodejs.org/en/) e um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/) <br> 
<br> 

<strong>...no terminal...</strong> 

Clone este repositório<br>

```
git clone <https://gitlab.com/apdaura/calculadora-testes> 
```
<strong>...no VsCode...</strong> 

Abra a pasta deste projeto

```
ctrl+K ctrl+O > localizar caminho onde a pasta foi clonada > selecionar pasta
```

Abra um terminal no Vscode

```
ctrl+shift+'
```

Execute os testes

```
npm test
```


<h1 id="autor">Autor</h1>
Entre em contato comigo pelo [Linkedin](www.linkedin.com/in/aline-priscila-daura), [Instagran](https://www.instagram.com/apdaura/?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D) ou pelo Whatsapp (14) 99850 - 4777 se tiver algum comentário, dúvida, preocupação ou apenas um desejo geral de papear comigo!

