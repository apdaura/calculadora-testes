## **Descrição do Bug**
Em uma conta que resulte em um número muito pequeno a calculadora considera apenas as últimas 16 casas decimais no pedido de apresentação da resposta

### Data do Incidente:
01/08/2023

### Erro/Status Fornecido:
1747917131966e-9
      + expected - actual

      -2.5811747917131966e-9
      +-2.5811747917131966e-9

### Passos para Reprodução:
Deve dividir 9 por 9 e dividir o resultado por 9 e assim por diante mais 8 vezes resultando em -2.5811747917131966e-9

### Comportamento Esperado:
O teste deveria passar e o número apresentado no resultado deveria ser -2.5811747917131966e-9

### Screenshot:
![ImagemDoBug](https://gitlab.com/apdaura/sprint1/uploads/f2bbc2e7a55a514b36b42f2cc9cbe18a/image.png)
