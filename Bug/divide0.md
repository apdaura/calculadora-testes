## **Descrição do Bug**
Em uma divisão por zero o programa da erro e o teste não passa
### Data do Incidente:
01/08/2023

### Erro/Status Fornecido:
AssertionError: expected Infinity to equal 'Não é possível dividir por zero'

### Passos para Reprodução:
Deve dividir qualquer número por zero

### Comportamento Esperado:
O teste deveria passar considerando uma mensagem de erro

### Screenshot:
![imagembug](image-1.png)