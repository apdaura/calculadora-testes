import assert from 'assert'
import chai from 'chai'
import Calculadora from '../src/Calculadora.js'

const expect = chai.expect

describe('teste de soma', () => {
    it('deve somar 4 e 5 resultando em 9', () => {
        let resultado = Calculadora.soma(4, 5)
        expect(resultado).to.be.equal(9)
    })
    it('deve somar -4 e 5 resultando em 1', () => {
        let resultado = Calculadora.soma(-4, 5)
        expect(resultado).to.be.equal(1)
    })
    it('deve somar -4 e -5 resultando em -9', () => {
        let resultado = Calculadora.soma(-4, -5)
        expect(resultado).to.be.equal(-9)
    })
    it('deve somar -4 e 4 resultando em 0', () => {
        let resultado = Calculadora.soma(-4, 4)
        expect(resultado).to.be.equal(0)
    })
    it('deve somar -0 e 0 resultando em 0', () => {
        let resultado = Calculadora.soma(-0, 0)
        expect(resultado).to.be.equal(0)
    })
    it('deve somar 4 e -0 resultando em 4', () => {
        let resultado = Calculadora.soma(4, -0)
        expect(resultado).to.be.equal(4)
    })
})

describe('Testes de subtração', () => {
    it('deve subtrair 4 e 5 resultando em -1', () => {
        let resultado = Calculadora.sub(4, 5)
        expect(resultado).to.be.equal(-1)
    })
    it('deve subtrair -4 e -5 resultando em -9', () => {
        let resultado = Calculadora.sub(4, 5)
        expect(resultado).to.be.equal(-9)
    })
    it('deve subtrair 4 e -5 resultando em -1', () => {
        let resultado = Calculadora.sub(4, -5)
        expect(resultado).to.be.equal(-1)
    })
    it('deve subtrair 0 e 0 resultando em 0', () => {
        let resultado = Calculadora.sub(0, 0)
        expect(resultado).to.be.equal(0)
    })
    it('deve subtrair 4 e -4 resultando em 0', () => {
        let resultado = Calculadora.sub(4, -4)
        expect(resultado).to.be.equal(8)
    })
})

describe('Testes de divisão', () => {
    it('deve dividir 4 e 0 resultando em Não é possível dividir por zero', () => {
        let resultado = Calculadora.div(4, 0)
        expect(resultado).to.be.equal('Não é possível dividir por zero')
    })
    it('deve dividir 0 e 4 resultando em 0', () => {
        let resultado = Calculadora.div(0, 4)
        expect(resultado).to.be.equal(0)
    })
    it('deve dividir -4 e 4 resultando em 0', () => {
        let resultado = Calculadora.div(-4, 4)
        expect(resultado).to.be.equal(-1)
    })
    it('deve dividir -5 e 4 resultando em 0', () => {
        let resultado = Calculadora.sub(0, 4)
        expect(resultado).to.be.equal(-1.25)
    })
    it('deve dividir 4 e 4 resultando em 0', () => {
        let resultado = Calculadora.div(4, 4)
        expect(resultado).to.be.equal(1)
    })

})

describe('Testes de multiplicação', () => {
    it('deve multiplicar 4 e -4 resultando em -16', () => {
        let resultado = Calculadora.mult(4, -4)
        expect(resultado).to.be.equal(-16)
    })
    it('deve multiplicar 0 e -4 resultando em 0', () => {
        let resultado = Calculadora.multi(0, -4)
        expect(resultado).to.be.equal(0)
    })
    it('deve multiplicar 1 e -4 resultando em -4', () => {
        let resultado = Calculadora.multi(1, -4)
        expect(resultado).to.be.equal(-4)
    })
    })

describe('Testes mistos', () => {
    it('deve dividir 8 por 2 e multiplicar por 5 e resultando em 20', () => {
        let resultado1 = Calculadora.div(8, 2)
        let resultado = Calculadora.mult(resultado1, 5)
        expect(resultado).to.be.equal(20)
    })    
    it('deve multiplicar 2 por 0,8 e subtrair por 1,6 e resultando em 0', () => {
        let resultado1 = Calculadora.mult(2, 0.8)
        let resultado = Calculadora.sub(resultado1, 1.6)
        expect(resultado).to.be.equal(0)
    }) 
    it('deve dividir 2 por 5 e subtrair por 0 e resultando em 0.4', () => {
        let resultado1 = Calculadora.div(2, 5)
        let resultado = Calculadora.sub(resultado1, 0)
        expect(resultado).to.be.equal(0.4)
    }) 
    it('deve multiplicar 9 por 9 e multiplicar o resultado por 9 e assim por diante mais 13 vezes e resultando em 31381059609', () => {
        let resultado1 = Calculadora.mult(9, 9)
        let resultado2 = Calculadora.mult(resultado1, 9)
        let resultado3 = Calculadora.mult(resultado2, 9)
        let resultado4 = Calculadora.mult(resultado3, 9)
        let resultado5 = Calculadora.mult(resultado4, 9)
        let resultado6 = Calculadora.mult(resultado5, 9)
        let resultado7 = Calculadora.mult(resultado6, 9)
        let resultado8 = Calculadora.mult(resultado7, 9)
        let resultado9 = Calculadora.mult(resultado8, 9)
        let resultado10 = Calculadora.mult(resultado9, 9)
        let resultado11 = Calculadora.mult(resultado10, 9)
        let resultado12 = Calculadora.mult(resultado11, 9)
        let resultado13 = Calculadora.mult(resultado12, 9)
        let resultado14 = Calculadora.mult(resultado13, 9)
        let resultado = Calculadora.mult(resultado14, 9)
        expect(resultado).to.be.equal(1853020188851841)
    })
    it('deve dividir 9 por 9 e dividir o resultado por 9 e assim por diante mais 8 vezes e resultando em -2.5811747917131966e-9', () => {
        let resultado1 = Calculadora.div(9, 9)
        let resultado2 = Calculadora.div(resultado1, 9)
        let resultado3 = Calculadora.div(resultado2, 9)
        let resultado4 = Calculadora.div(resultado3, 9)
        let resultado5 = Calculadora.div(resultado4, 9)
        let resultado6 = Calculadora.div(resultado5, 9)
        let resultado7 = Calculadora.div(resultado6, 9)
        let resultado8 = Calculadora.div(resultado7, 9)
        let resultado9 = Calculadora.div(resultado8, 9)
        let resultado = Calculadora.div(resultado9, 9)
        expect(resultado).to.be.equal(-2.5811747917131966e-9)
    })
    it('deve dividir 9 por 9 e somar 5 e multiplicar por 2 e subtrair 4 ', () => {
        let resultado1 = Calculadora.div(9, 9)
        let resultado2 = Calculadora.soma(resultado1, 5)
        let resultado3 = Calculadora.mult(resultado2, 2)         
        let resultado = Calculadora.sub(resultado3, 4)
        expect(resultado).to.be.equal(8)
    })

    
})
